# PUSL2003 Coursework - Individual Contribution Progress Report #

# NSBM Hub Management System (Program Office Desktop Application) #

# 35% Completed Project of NSBM Hub Management System Uploaded for review from private repository. #

NSBM Hub Management System developed by H.V.L.Hasanka - 10673969

# HOW TO RUN THIS APPLICATION #
1. Open this folder through CMD (terminal). Command: cd <folder pathway>
2. Type the following command: npm run start
3. And click enter.
If all the required resources are available the application should be running on the localhost:4200. Check this from any preferred web browser.

# Commit Legend - #
S <Commit Number> = State <Commit Number>
